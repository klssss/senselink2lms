# gevent==20.6.2
requests==2.23.0
python-dotenv==0.10.5
Flask==1.1.2
Flask-SocketIO==4.3.0
gevent-websocket==0.10.1
numpy==1.16.4
pytz==2019.2
Flask-RESTful==0.3.8               
Flask-SocketIO==4.3.0 
