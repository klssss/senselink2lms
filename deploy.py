import argparse
import os
import getpass
import sys

# get current working directory
homedir = os.environ['HOME']
cwd = os.getcwd()
current_directory_name = cwd.split('/')[-1]

# set argparse
ap = argparse.ArgumentParser()
ap.add_argument("-N", "--name", 
                required=False, 
                default=current_directory_name,
                type=str,
                help="service name")

ap.add_argument("-s", "--should-send-to-lms", 
                required=False, 
                default=True, 
                action='store_true')

ap.add_argument("-n", '--no-send', 
                required=False, 
                dest="should_send_to_lms", 
                action='store_false',
                help="do not send data to lms")

ap.add_argument("-L", "--lms", 
                required=False, 
                default='eclass', 
                help="learning management platform (eclass or grwth)")

ap.add_argument("-r", "--remote", 
                required=False, 
                default=True, 
                action='store_true',
                help="executing in a different subnet in regards to SenseLink")
                
ap.add_argument("-l", '--local', 
                required=False, 
                dest="remote", 
                action='store_false',
                help="executing in the same subnet in regards to SenseLink")

ap.add_argument("-v", '--verbose', 
                required=False, 
                default=False, 
                action='store_true',
                help="print more messages for debugging")

ap.add_argument("-f", '--fire-alarm-open-all-doors', 
                required=False, 
                default=False, 
                action='store_true',
                help="print more messages for debugging")

# get service name
args = vars(ap.parse_args())
app_name = args["name"]

# the lms used
lms = args["lms"]
#send data to lms or not
should_send_to_lms = args["should_send_to_lms"]
# executing remotely or locally in regards to senselink
remote = args["remote"]
# verbose: print more debug messages
verbose = args["verbose"]

# get username
user = getpass.getuser()

# write service
f = open(f"{app_name}.service", "w")
f.write(f"""[Unit]
Description={app_name}
After=network.target

[Service]
ExecStart={cwd}/senselink2lms.sh
WorkingDirectory={cwd}
StandardOutput=inherit
StandardError=inherit
Restart=always
User={user}
Environment=XAUTHORITY={homedir}/.Xauthority
Environment=DISPLAY=:0.0

[Install]
WantedBy=multi-user.target
""")
f.close()

# write sh
f = open(f"senselink2lms.sh", "w")
f.write(f"""#!/bin/bash
python{sys.version_info[0]}.{sys.version_info[1]} -u push_event_main.py {"" if should_send_to_lms else "--no-send"} {"" if remote else "--local"} --lms {lms} {"--verbose" if verbose else ""} >> default.log 2>&1
""")
f.close()

os.chmod("senselink2lms.sh", 0o744)

# enable and start service
print(os.popen(f"sudo cp {app_name}.service /etc/systemd/system/{app_name}.service").read())
print(os.popen(f"sudo systemctl enable {app_name}").read())
print(os.popen(f"sudo systemctl restart {app_name}").read())
