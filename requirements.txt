pandas==1.0.3
requests==2.23.0
python-dotenv==0.10.5
PyQt5==5.14
numpy==1.16.4
pytz==2019.2