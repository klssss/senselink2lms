import schedule
import time
import os
import requests
import hashlib
import json
import pandas as pd
import random
import numpy as np
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel
from PyQt5.QtGui import QFont, QFontMetrics
from PyQt5.QtCore import QThread, Qt
from threading import Thread
import os
from datetime import datetime

#Input app key
app_key = "39aa91bcb884de1e"
#Input app secret
app_secret = "8d14e201ffb8292e533d23ba90b593e8"
#Image path
path = ""
#API URL
uri = "/api/v3/record/list"

temperature_threshold = 38

def job(app):
    # print("I'm working...")

    os.system('python get_records_json.py')
    os.system('python preprocess_json_to_csv_for_visualization.py')
    os.system('python print_statistics.py')

    # os.system('python visualize.py')

def getTimestamp():
    return str(round(time.time()) * 1000)

def getSign(timestamp):
    hl = hashlib.md5()
    hl.update((timestamp + '#' +app_secret).encode(encoding='utf-8'))
    return hl.hexdigest()

def find_nearest_temp_top(idx, item, df):
    if idx == 0:
        if item['body_temperature'] is not None and item['user_id'] == 0:
            return item['body_temperature'], 0 #somehow need ", 0" here
        else:
            return 37, 0
        # return 37,0
    elif df.iloc[idx-1]['body_temperature'] is not None and df.iloc[idx-1]['user_id'] == 0 :
        return df.iloc[idx-1]['body_temperature'], idx-1
    else:
        return find_nearest_temp_top(idx-1, item, df)

def find_nearest_temp_down(idx, item, df):

    if idx == len(df)-1:
        if item['body_temperature'] is not None and item['user_id'] == 0:
            return item['body_temperature'], 0 #somehow need ", 0" here
        else:
            return 37, len(df)-1
    elif df.iloc[idx+1]['body_temperature'] is not None and df.iloc[idx+1]['user_id'] == 0 :
        return df.iloc[idx+1]['body_temperature'], idx+1
    else:
        return find_nearest_temp_down(idx+1, item, df)


def compare(idx, item, df, top_idx, down_idx):

    top_time_diff = df.iloc[top_idx]['sign_time'] - item['sign_time']
    down_time_diff = item['sign_time'] - df.iloc[down_idx]['sign_time']

    if top_time_diff <= down_time_diff:
        return 1
    else:
        return 0

def time_range_exceed_four_seconds(df, idx, compare_idx):

    time_diff = abs(df.iloc[idx]['sign_time'] - df.iloc[compare_idx]['sign_time'])
    if time_diff >= 4:
        return True
    else:
        return False



class Main_GUI(QWidget):
    def __init__(self):
        super().__init__()
        self.resize(800, 400)

        layout = QVBoxLayout()
        self.setLayout(layout)

        current_layout = QVBoxLayout()
        prev1_layout = QVBoxLayout()
        prev2_layout = QVBoxLayout()

        layout.addLayout(current_layout)
        layout.addLayout(prev1_layout)
        layout.addLayout(prev2_layout)
        
        current_layout.addStretch(1)

        current_name_temperature_font = QFont('Open Sans', 70, QFont.Bold)
        current_time_font = QFont('Open Sans', 35, QFont.Bold)

        self.current_name_temperature_label = QLabel()
        self.current_name_temperature_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
        self.current_name_temperature_label.setFont(current_name_temperature_font)
        current_layout.addWidget(self.current_name_temperature_label)

        self.current_time_label = QLabel()
        self.current_time_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
        self.current_time_label.setFont(current_time_font)
        current_layout.addWidget(self.current_time_label)

        # self.current_name_temperature_label.setText("Name Temp")

        prev1_layout.addStretch(1)

        prev_name_temperature_font = QFont('Open Sans', 40, QFont.Bold)
        prev_time_font = QFont('Open Sans', 20, QFont.Bold)

        self.prev1_name_temperature_label = QLabel()
        self.prev1_name_temperature_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
        self.prev1_name_temperature_label.setFont(prev_name_temperature_font)
        prev1_layout.addWidget(self.prev1_name_temperature_label)

        self.prev1_time_label = QLabel()
        self.prev1_time_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
        self.prev1_time_label.setFont(prev_time_font)
        prev1_layout.addWidget(self.prev1_time_label)

        # self.prev1_name_temperature_label.setText("Name Temp")

        self.prev2_name_temperature_label = QLabel()
        self.prev2_name_temperature_label.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
        self.prev2_name_temperature_label.setFont(prev_name_temperature_font)
        prev2_layout.addWidget(self.prev2_name_temperature_label)

        self.prev2_time_label = QLabel()
        self.prev2_time_label.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
        self.prev2_time_label.setFont(prev_time_font)
        prev2_layout.addWidget(self.prev2_time_label)

        prev2_layout.addStretch(1)

        # self.prev2_name_temperature_label.setText("Name Temp")

current_name_temperature_time = ('', None)
prev_name_temperature_time1 = ('', None)
prev_name_temperature_time2 = ('', None)

count = 0
def all():
    global current_name_temperature_time
    global prev_name_temperature_time1
    global prev_name_temperature_time2
    global count

    count += 1
    print(count)

    timestamp = getTimestamp()
    sign = getSign(timestamp)
    url = "http://10.113.176.92" + uri
    data = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
        #"date_time_from": str(round(time.time()-10000)*1000),
        #"date_time_to": str(round(time.time())*1000)
    }
    # files = {
    #     "face_avatar": (str.split(path, "/")[-1], open(path, "rb"), "image/jpeg")
    # }

    result = requests.get(url, params=data)

    # print(result.text)

    #result = json.dumps(result.text, indent=4)

    data = json.loads(result.text, encoding = 'utf_8_sig')

    #df = pd.read_json(data['data']['data']['record_list'])

    df = pd.DataFrame.from_records(data['data']['data']['record_list'])

    df_new_list = []

    for idx, item in df.iterrows():
        if item['entry_mode'] == 1 and item['groups'] is not None:
            df_new_list.append(item)
        if item['entry_mode'] == 3:

            if idx == 0:
                down, down_idx = find_nearest_temp_down(idx, item, df)
                temporary = item
                exceed5 = time_range_exceed_four_seconds(df, idx, down_idx)
                if exceed5:
                    temporary['body_temperature'] = 'No records in 4 sec'
                else:
                    temporary['body_temperature'] = down
                df_new_list.append(temporary)

            elif idx == len(df) - 1:
                top, top_idx = find_nearest_temp_top(idx, item, df)
                temporary = item
                exceed5 = time_range_exceed_four_seconds(df, idx, top_idx)
                if exceed5:
                    temporary['body_temperature'] = 'No records in 4 sec'
                else:
                    temporary['body_temperature'] = top
                df_new_list.append(temporary)

            else:
                top, top_idx = find_nearest_temp_top(idx, item, df)
                down, down_idx = find_nearest_temp_down(idx, item, df)
                flag_use_top = compare(idx, item, df, top_idx, down_idx)

                if flag_use_top == 1:
                    temporary = item
                    exceed5 = time_range_exceed_four_seconds(df, idx, top_idx)
                    if exceed5:
                        temporary['body_temperature'] = 'No records in 4 sec'
                    else:
                        temporary['body_temperature'] = top
                    df_new_list.append(temporary)
                else:
                    temporary = item
                    exceed5 = time_range_exceed_four_seconds(df, idx, top_idx)
                    if exceed5:
                        temporary['body_temperature'] = 'No records in 4 sec'
                    else:
                        temporary['body_temperature'] = top
                    df_new_list.append(temporary)

    df_new = pd.DataFrame(df_new_list)

    # print(datetime.fromtimestamp(df_new.iloc[0]['sign_time']).strftime('%H:%M:%S'))

    # sys.stdout.write("\033[K")
    # print(df_new.iloc[0]['user_name'],
    #       df_new.iloc[0]['user_ic_number'],
    #       df_new.iloc[0]['body_temperature'],
    #       end='\r')

    # return if no data is retrieved
    if len(df_new.index) < 1:
        return

    # do not display No records in 4 sec
    if df_new.iloc[0]['body_temperature'] == 'No records in 4 sec':
        return

    # do not display QRCode
    if df_new.iloc[0]['entry_mode'] == 2:
        return
    
    # round and convert temperature to string if needed
    if type(df_new.iloc[0]['body_temperature']) is not str:
        temperature = str(round(df_new.iloc[0]['body_temperature'], 1))
    else:
        temperature = df_new.iloc[0]['body_temperature']

    # if new data is coming
    if current_name_temperature_time != (str(df_new.iloc[0]['user_name']), temperature, str(datetime.fromtimestamp(df_new.iloc[0]['sign_time']).strftime('%H:%M:%S'))):

        # play alarm if needed (need to install sox: sudo apt install sox)
        try:
            if float(temperature) >= temperature_threshold:
                for i in range(4):
                    os.system('play -nq -t alsa synth {} sine {}'.format(0.1, 880))
                    time.sleep(0.05)
                os.system('play -nq -t alsa synth {} sine {}'.format(0.1, 880))
        except:
            pass

        #update UI
        prev_name_temperature_time2 = prev_name_temperature_time1
        prev_name_temperature_time1 = current_name_temperature_time
        current_name_temperature_time = (str(df_new.iloc[0]['user_name']), temperature, str(datetime.fromtimestamp(df_new.iloc[0]['sign_time']).strftime('%H:%M:%S')))

        update_labels(main_gui.current_name_temperature_label, main_gui.current_time_label, current_name_temperature_time)
        update_labels(main_gui.prev1_name_temperature_label, main_gui.prev1_time_label, prev_name_temperature_time1)
        update_labels(main_gui.prev2_name_temperature_label, main_gui.prev2_time_label, prev_name_temperature_time2)

        #send data to eclass
        if df_new.iloc[0]['ic_number']:
            endpoint = "https://eclass-api2.eclasscloud.hk/api/student-attendance"
            card_id = str(df_new.iloc[0]['ic_number']).zfill(10)
            record_date = str(df_new.iloc[0]['sign_date'])
            record_time = str(datetime.fromtimestamp(df_new.iloc[0]['sign_time']).strftime('%H:%M:%S'))
            temperature = float(df_new.iloc[0]['body_temperature'])
            temperature_status = 1 if temperature >= temperature_threshold else 0
            request_type = "ATT"
            licence_key = "zMoknwMQANCapt2pjDr1gaUcoQIxEC5Y6zUltoKeUKNpeflEN07EMkzEJzG4CdOdtpodsDMhcOJkdoxE"
            data = {"card_id": card_id,
                    "record_date": record_date,
                    "record_time": record_time,
                    "temperature": temperature,
                    "temperature_status": temperature_status,
                    "request_type": request_type}
            headers = {"Authorization": "Bearer " + licence_key}

            response = requests.post(endpoint, data=data, headers=headers)
            print(response)
            print(response.text)



# Subclassing QThread
# http://qt-project.org/doc/latest/qthread.html
class Update_info_thread(QThread):
    def run(self):

        '''
        # init for demo purposes only
        import random
        names = ['Polo', 'Andy', 'Jack']
        temperature = 0

        # define original placeholders
        current_name_temperature_time = ('', None)
        prev_name_temperature_time1 = ('', None)
        prev_name_temperature_time2 = ('', None)

        while True:
            # save previous info
            prev_name_temperature_time2 = prev_name_temperature_time1
            prev_name_temperature_time1 = current_name_temperature_time

            # generate fake data for demo purposes only
            name = random.choice(names)
            temperature = round(random.uniform(35.5,37.5), 1)

            # get current info
            current_name_temperature_time = (name, temperature)
            
            # update gui
            update_labels(main_gui.current_name_temperature_label, current_name_temperature_time)
            update_labels(main_gui.prev1_name_temperature_label, prev_name_temperature_time1)
            update_labels(main_gui.prev2_name_temperature_label, prev_name_temperature_time2)

            # sleep for demo purposes only
            time.sleep(1)
        '''
        
        schedule.every(0.5).seconds.do(all)
        while 1:
            schedule.run_pending()
            # time.sleep(0.5)
        
def update_labels(name_temperature_label, time_label, name_temperature_time):
    if name_temperature_time[1]:
        name_temperature_label.setText(name_temperature_time[0] + " " + str(name_temperature_time[1]) + "°C")
        time_label.setText(name_temperature_time[2])
        try:
            temperature = float(name_temperature_time[1])
            if float(name_temperature_time[1]) >= temperature_threshold:
                name_temperature_label.setStyleSheet("background-color:#ff0000;")
            else:
                name_temperature_label.setStyleSheet("background-color: rgba(0,0,0,0%)")
        except:
            name_temperature_label.setStyleSheet("background-color: rgba(0,0,0,0%)")

app = QApplication([])
main_gui = Main_GUI()
main_gui.show()
thread = Update_info_thread()
thread.finished.connect(app.exit)
thread.start()
app.exit(app.exec_())