from time import time, sleep
import os
import requests
import hashlib
import json
import pandas as pd
import random
import numpy as np
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QMainWindow, QComboBox
from PyQt5.QtGui import QFont, QFontMetrics
from PyQt5.QtCore import QThread, Qt
from threading import Thread
import os
from datetime import datetime, date
import traceback

#host
host = "http://192.168.0.151"
#Input app key
app_key = "3c02403cfcf21ef0"
#Input app secret
app_secret = "173c4648b688d4c1487a698bb71ad9f1"
#Image path
path = ""
#API URL
uri = "/api/v3/record/list"
#eclass licence key
eclass_licence_key = "5|Z0V54dxCg5a3iCGE33yyh4qsMVkIXE0hDZfk6zWtgFoHTtg8QYiG8915kXc5KyXXaD3L86rMWVZea7rB"
# eclass attendence api endpoint
eclass_api_endpoint = "https://eclass-api2.eclasscloud.hk/api/student-attendance"

#device name
current_device_id = 'SPSE-23f8534fcdffc1ecd0d68cad585847a8'

def get_devices():
    timestamp = getTimestamp()
    sign = getSign(timestamp)
    url = host + '/api/v3/device'
    data = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
    }

    success = False
    while not success:
        try:
            result = requests.get(url, params=data)
            success = True
        except requests.exceptions.ConnectionError:
            print(f"cannot connect to {url}, check internet connection?")
            sleep(1)

    while not result.ok:
        sleep(1)
        print('looks like senselink is not starting but not ready yet, please wait for a few more minutes')
    data = json.loads(result.text, encoding = 'utf_8_sig')

    return [{'ldid': device['ldid'], 'name': device['name']} for device in data['data']['device_list']]

#late time threshold
def get_late_time_threshold():
    return datetime.now().replace(hour=8, minute=0, second=0, microsecond=0)

#temperature threshold
temperature_threshold = 38

#list storing user names
names = []

#today's date
today = date.today()

#for filtering out first record
first_data = True

def job(app):
    # print("I'm working...")

    os.system('python get_records_json.py')
    os.system('python preprocess_json_to_csv_for_visualization.py')
    os.system('python print_statistics.py')

    # os.system('python visualize.py')

def getTimestamp():
    return str(round(time()) * 1000)

def getSign(timestamp):
    hl = hashlib.md5()
    hl.update((timestamp + '#' +app_secret).encode(encoding='utf-8'))
    return hl.hexdigest()

def find_nearest_temp_top(idx, item, df):
    if idx == 0:
        if item['body_temperature'] is not None and item['body_temperature'] != 0.0 and item['user_id'] == 0:
            return item['body_temperature'], 0
        else:
            return 37, 0
    elif df.iloc[idx-1]['body_temperature'] is not None and df.iloc[idx-1]['body_temperature'] != 0.0 and df.iloc[idx-1]['user_id'] == 0 :
        return df.iloc[idx-1]['body_temperature'], idx-1
    else:
        return find_nearest_temp_top(idx-1, item, df)

def find_nearest_temp_down(idx, item, df):
    if idx == len(df)-1:
        if item['body_temperature'] is not None and item['body_temperature'] != 0.0 and item['user_id'] == 0:
            return item['body_temperature'], len(df)-1
        else:
            return 37, len(df)-1
    elif df.iloc[idx+1]['body_temperature'] is not None and df.iloc[idx+1]['body_temperature'] != 0.0 and df.iloc[idx+1]['user_id'] == 0 :
        return df.iloc[idx+1]['body_temperature'], idx+1
    else:
        return find_nearest_temp_down(idx+1, item, df)


def compare(idx, item, df, top_idx, down_idx):
    top_time_diff = df.iloc[top_idx]['sign_time'] - item['sign_time']
    down_time_diff = item['sign_time'] - df.iloc[down_idx]['sign_time']

    if top_time_diff <= down_time_diff:
        return 1
    else:
        return 0

def time_range_exceed_four_seconds(df, idx, compare_idx):
    time_diff = abs(df.iloc[idx]['sign_time'] - df.iloc[compare_idx]['sign_time'])
    if time_diff >= 4:
        return True
    else:
        return False

class Main_GUI(QMainWindow):
    def __init__(self):
        super().__init__()
        self.resize(800, 400)

        # self.setStyleSheet("background-color:#ff0000;")

        self.centralWidget = QWidget()
        self.setCentralWidget(self.centralWidget)

        layout = QVBoxLayout()
        self.centralWidget.setLayout(layout)

        devices_select_layout = QHBoxLayout()
        current_layout = QVBoxLayout()
        head_count_layout = QHBoxLayout()
        #prev1_layout = QVBoxLayout()
        #prev2_layout = QVBoxLayout()

        layout.addLayout(devices_select_layout)
        layout.addStretch(1)
        layout.addLayout(current_layout)
        layout.addLayout(head_count_layout)
        layout.addStretch(1)
        #layout.addLayout(prev1_layout)
        #layout.addLayout(prev2_layout)

        device_label = QLabel()
        device_label.setText("Device: ")
        self.device_selection_box = QComboBox()
        width = self.device_selection_box.sizeHint().width()
        if width < 200:
            self.device_selection_box.setFixedWidth(200)
        self.device_selection_box.addItems([device['name'] for device in devices])
        self.device_selection_box.currentIndexChanged.connect(self.device_selection_change)

        devices_select_layout.addStretch(1)
        devices_select_layout.addWidget(device_label)
        devices_select_layout.addWidget(self.device_selection_box)

        current_name_temperature_font = QFont('Open Sans', 70, QFont.Bold)
        current_time_font = QFont('Open Sans', 50, QFont.Bold)

        self.current_name_temperature_label = QLabel()
        self.current_name_temperature_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
        self.current_name_temperature_label.setFont(current_name_temperature_font)
        current_layout.addWidget(self.current_name_temperature_label)

        self.current_time_label = QLabel()
        self.current_time_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
        self.current_time_label.setFont(current_time_font)
        current_layout.addWidget(self.current_time_label)

        head_count_font = QFont('Open Sans', 30, QFont.Bold)
        self.head_count_label = QLabel()
        self.head_count_label.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.head_count_label.setFont(head_count_font)
        self.head_count_label.setText('人數: ' + str(len(names)))
        head_count_layout.addStretch(5)
        head_count_layout.addWidget(self.head_count_label)
        head_count_layout.addStretch(1)

        '''
        prev1_layout.addStretch(1)

        prev_name_temperature_font = QFont('Open Sans', 40, QFont.Bold)
        prev_time_font = QFont('Open Sans', 30, QFont.Bold)

        self.prev1_name_temperature_label = QLabel()
        self.prev1_name_temperature_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
        self.prev1_name_temperature_label.setFont(prev_name_temperature_font)
        prev1_layout.addWidget(self.prev1_name_temperature_label)

        self.prev1_time_label = QLabel()
        self.prev1_time_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
        self.prev1_time_label.setFont(prev_time_font)
        prev1_layout.addWidget(self.prev1_time_label)

        self.prev2_name_temperature_label = QLabel()
        self.prev2_name_temperature_label.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
        self.prev2_name_temperature_label.setFont(prev_name_temperature_font)
        prev2_layout.addWidget(self.prev2_name_temperature_label)

        self.prev2_time_label = QLabel()
        self.prev2_time_label.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
        self.prev2_time_label.setFont(prev_time_font)
        prev2_layout.addWidget(self.prev2_time_label)

        prev2_layout.addStretch(1)
        '''

    def device_selection_change(self, index):
        global current_device_id
        global first_data
        current_device_id = devices[index]['ldid']
        first_data = True

current_name_temperature_datetime = ('', None, None)
prev_name_temperature_datetime1 = ('', None, None)
prev_name_temperature_datetime2 = ('', None, None)

def all():
    global current_name_temperature_datetime
    global prev_name_temperature_datetime1
    global prev_name_temperature_datetime2
    global today
    global names
    global first_data

    timestamp = getTimestamp()
    sign = getSign(timestamp)
    url = host + uri
    data = {
        "app_key": app_key,
        "sign": sign,
        "timestamp": timestamp,
        "device_sn": current_device_id,
        #"date_time_from": str(round(time()-10000)*1000),
        #"date_time_to": str(round(time())*1000)
    }
    # files = {
    #     "face_avatar": (str.split(path, "/")[-1], open(path, "rb"), "image/jpeg")
    # }

    success = False
    while not success:
        try:
            result = requests.get(url, params=data)
            success = True
        except requests.exceptions.ConnectionError:
            print(f"cannot connect to {url}, check internet connection?")
            sleep(1)

    # print(result.text)

    #result = json.dumps(result.text, indent=4)

    data = json.loads(result.text, encoding = 'utf_8_sig')

    #df = pd.read_json(data['data']['data']['record_list'])

    df = pd.DataFrame.from_records(data['data']['data']['record_list'])

    df_new_list = []

    for idx, item in df.iterrows():
        if item['entry_mode'] == 1 and item['groups'] is not None:
            # recognized face
            df_new_list.append(item)
        if item['entry_mode'] == 3:
            # recognized card
            if idx == 0:
                #first item
                down, down_idx = find_nearest_temp_down(idx, item, df)
                temporary = item
                exceed5 = time_range_exceed_four_seconds(df, idx, down_idx)
                if not exceed5:
                    temporary['body_temperature'] = down
                df_new_list.append(temporary)

            elif idx == len(df) - 1:
                top, top_idx = find_nearest_temp_top(idx, item, df)
                temporary = item
                exceed5 = time_range_exceed_four_seconds(df, idx, top_idx)
                if not exceed5:
                    temporary['body_temperature'] = top
                df_new_list.append(temporary)

            else:
                top, top_idx = find_nearest_temp_top(idx, item, df)
                down, down_idx = find_nearest_temp_down(idx, item, df)
                flag_use_top = compare(idx, item, df, top_idx, down_idx)

                if flag_use_top == 1:
                    temporary = item
                    exceed5 = time_range_exceed_four_seconds(df, idx, top_idx)
                    if not exceed5:
                        temporary['body_temperature'] = top
                    df_new_list.append(temporary)
                else:
                    temporary = item
                    exceed5 = time_range_exceed_four_seconds(df, idx, down_idx)
                    if not exceed5:
                        temporary['body_temperature'] = down
                    df_new_list.append(temporary)

    df_new = pd.DataFrame(df_new_list)

    # print(datetime.fromtimestamp(df_new.iloc[0]['sign_time']).strftime('%H:%M:%S'))

    # sys.stdout.write("\033[K")
    # print(df_new.iloc[0]['user_name'],
    #       df_new.iloc[0]['user_ic_number'],
    #       df_new.iloc[0]['body_temperature'],
    #       end='\r')

    #if today is a new day, then clear the remembered names
    if today != date.today():
        names = []
        today = date.today()
        main_gui.head_count_label.setText("人數: " + str(len(names)))

    # return if no data is retrieved
    if len(df_new.index) < 1:
        return

    # do not display QRCode
    if df_new.iloc[0]['entry_mode'] == 2:
        return
    
    # if new data is coming
    if current_name_temperature_datetime != (str(df_new.iloc[0]['user_name']), df_new.iloc[0]['body_temperature'], datetime.fromtimestamp(df_new.iloc[0]['sign_time'])):
        # update data
        prev_name_temperature_datetime2 = prev_name_temperature_datetime1
        prev_name_temperature_datetime1 = current_name_temperature_datetime
        current_name_temperature_datetime = (str(df_new.iloc[0]['user_name']), df_new.iloc[0]['body_temperature'], datetime.fromtimestamp(df_new.iloc[0]['sign_time']))
        name, temperature, _datetime = current_name_temperature_datetime
        
        #ignore first data read
        if first_data:
            first_data = False
            return

        # remember who checked-in
        if name not in names:
            names.append(name)

        # play alarm if needed (need to install sox: sudo apt install sox)
        if temperature and temperature >= temperature_threshold:
            for i in range(4):
                os.system('play -nq -t alsa synth {} sine {}'.format(0.1, 880))
                sleep(0.05)
            os.system('play -nq -t alsa synth {} sine {}'.format(0.1, 880))

        #update UI
        update_labels(main_gui.current_name_temperature_label, main_gui.current_time_label, *current_name_temperature_datetime)
        #update_labels(main_gui.prev1_name_temperature_label, main_gui.prev1_time_label, *prev_name_temperature_datetime1)
        #update_labels(main_gui.prev2_name_temperature_label, main_gui.prev2_time_label, *prev_name_temperature_datetime2)
        main_gui.head_count_label.setText("人數: " + str(len(names)))

        #send data to eclass
        if df_new.iloc[0]['ic_number']:
            card_id = str(df_new.iloc[0]['ic_number']).zfill(10)
            record_date = str(df_new.iloc[0]['sign_date'])
            record_time = str(datetime.fromtimestamp(df_new.iloc[0]['sign_time']).strftime('%H:%M:%S'))
            request_type = "ATT"
            data = {"card_id": card_id,
                    "record_date": record_date,
                    "record_time": record_time,
                    "request_type": request_type}

            # include temperature data if present
            temperature = df_new.iloc[0]['body_temperature']
            if temperature:
                temperature_status = 1 if temperature >= temperature_threshold else 0
                data["temperature"] = temperature
                data["temperature_status"] = temperature_status

            # include locartion data if present
            location = df_new.iloc[0]['location']
            if location:
                data["location"] = location[:16]
            
            try:
                print("data:", json.dumps(data, indent=4))
            except TypeError:
                print("somehow can't print data in json format")
                print("give up and not send this record to eclass")
                print(data)
                traceback.print_exc()
                return

            
            headers = {"Authorization": "Bearer " + eclass_licence_key}

            success = False
            while not success:
                try:
                    response = requests.post(eclass_api_endpoint, data=data, headers=headers)
                    success = True
                except requests.exceptions.ConnectionError:
                    print(f"cannot connect to {eclass_api_endpoint}, check internet connection?")
                    sleep(1)

            print(response)
            print(response.text)

# Subclassing QThread
# http://qt-project.org/doc/latest/qthread.html
class Update_info_thread(QThread):
    def run(self):
        while True:
            all()
            sleep(0.5)
        
def update_labels(name_temperature_label, time_label, name, temperature, datetime):
    # name_temperature_label.setText(name + "\n" + str(temperature) + "°C")
    time_label.setText(datetime.strftime('%H:%M:%S'))

    #display name, temperature
    if not temperature:
        name_temperature_label.setText(name + "\n" + "")
    elif temperature >= temperature_threshold:
        name_temperature_label.setText(name + "\n" + "體溫異常")
    else:
        name_temperature_label.setText(name + "\n" + "體溫正常")

    # highlight name red for high temperature
    if not temperature:
        name_temperature_label.setStyleSheet("background-color: rgba(0,0,0,0%)")
    elif temperature >= temperature_threshold:
        name_temperature_label.setStyleSheet("background-color:#ff0000;")
    else:
        name_temperature_label.setStyleSheet("background-color: rgba(0,0,0,0%)")
    
    # highlight time green for late
    '''
    late_time_threshold = get_late_time_threshold()
    if datetime > late_time_threshold:
        time_label.setStyleSheet("background-color:#00ff00;")
    else:
        time_label.setStyleSheet("background-color: rgba(0,0,0,0%)")
    '''   

devices = get_devices()
current_device_id = devices[0]['ldid']

stylesheet = """
    Main_GUI {
    border-image: url("placeholder.jpeg");  
}
"""

app = QApplication([])
app.setStyleSheet(stylesheet)
main_gui = Main_GUI()
main_gui.show()
thread = Update_info_thread()
thread.finished.connect(app.exit)
thread.start()
app.exit(app.exec_())